'use-strict'

import { exit } from 'process'

import { stations, capabilities, auth } from './firebase'

import Station from './models/Station'
import CapabilityManager from './models/CapabilityManager'

import exampleHook from './hooks/example'
import { temperatureHook, humidityHook, pressureHook } from './hooks/example-bme280'

// ENTER FIREBASE USERNAME AND PASSWORD - FOR DEVELOPMENT ONLY. DO NOT COMMIT!

const username = ''
const password = ''

;(async function () {
  const user = await auth.signInWithEmailAndPassword(username, password).catch(error => {
    console.log('Unable to sign in: ' + error.message)
    exit(1)
  })

  if (user) {
    console.log(`Logged in: ${username}`)
  } else {
    console.log(`Unable to log in as: ${username}`)
    exit(1)
  }

  // Hard coded station reference
  const reference = stations.doc('10000000536c58bd')
  const station = new Station(reference)

  // Boilerplate DataHook
  const temperatureManager = new CapabilityManager(capabilities.doc('temperature'), exampleHook)

  station.activate(temperatureManager)

  station.update()
})()
