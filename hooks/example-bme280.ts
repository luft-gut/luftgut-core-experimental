import DataHook from '../models/DataHook'
const bme280 = require('bme280')

/**
 * Gets a data object from the sensor
 */
async function getData(): Promise<any> {
  const sensor = await bme280.open()
  const data = await sensor.read()

  await sensor.close()

  return data
}

/**
 * Returns the temperature in Celsius
 */
const temperatureHook: DataHook<number> = async () => {
  const data = await getData()
  return data.temperature
}

/**
 * Returns the pressure hPa
 */
const pressureHook: DataHook<number> = async () => {
  const data = await getData()
  return data.pressure
}

/**
 * Returns the humidity in percent
 */
const humidityHook: DataHook<number> = async () => {
  const data = await getData()
  return data.humidity
}

export { temperatureHook, pressureHook, humidityHook }
