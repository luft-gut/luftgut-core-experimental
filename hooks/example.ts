import DataHook from '../models/DataHook'

/**
 * Returns a random number between 0 and 100
 */
const exampleHook: DataHook<number> = async () => {
  return Math.random() * 100
}

export default exampleHook
